# TypOn - The Microbial Typing Ontology

TyPon provides a comprehensive description of the existing microbial typing methods for the identification of bacterial Isolates and their classification. Such a description constitutes an universal format for the exchange of information on the microbial typing field, providing a vehicle for the integration of the numerous disparate online databases. In its current version, TyPon describes most used microbial typing methods but it is, and always will be, a work in progress given the constant advances in the microbial typing field.
